﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ejercicio1 : MonoBehaviour
{
    public bool haveKey = false;
    [SerializeField]
    Rigidbody rb;
    bool moved = false;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        float v = Input.GetAxisRaw("Vertical");
        float h = Input.GetAxisRaw("Horizontal");
        Vector3 force = new Vector3(0, 0, v);
        Vector3 angle = new Vector3(0, h, 0);

        rb.AddForce(force* 1000 * Time.fixedDeltaTime);
        //Que coja un input. Mover a un personaje a través de fuerzas y con inputs con físicas

        //rb.AddForce(transform.forward*500*Time.fixedDeltaTime);
        rb.AddTorque(angle * 1000 * Time.fixedDeltaTime);
    }

    private void OnCollisionEnter(Collision collision){

        if (collision.gameObject.layer == 8 && collision.gameObject.tag == "Pared")
        {
            new WaitForSeconds(1);
            Rigidbody otherRb = collision.gameObject.GetComponent<Rigidbody>();
            
            otherRb.MovePosition(new Vector3(-10, collision.other.transform.position.y, collision.other.transform.position.z));
            moved = true;
            Debug.Log("He colisionado con " + collision.gameObject.name + " a una velocidad relativa de " + collision.relativeVelocity);
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.layer == 8 && collision.gameObject.tag == "Pared")
            Debug.Log("Deje de colisionar con " + collision.gameObject.name);
    }
    private void OnTriggerEnter(Collider other){
        Debug.Log("He entrado en " + other.name + " en una posición de "+other.transform.position);
    }
    private void OnTriggerStay(Collider other)
    {
        if(Input.GetKey(KeyCode.Space))
            Debug.Log("Estoy en un trigger");
    }
}
