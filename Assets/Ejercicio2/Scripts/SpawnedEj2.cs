﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnedEj2 : MonoBehaviour
{
    Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        float x = 10 * (Random.value - 0.5f);
        float y = 4 * Random.value;
        float z = 10 * (Random.value - 0.5f);
        rb = GetComponent<Rigidbody>();
        rb.AddForce(new Vector3(x, y, z) * 10, ForceMode.Impulse);
    }
}
