﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEj2 : MonoBehaviour
{
    /// <summary>
    ///
    /// 1. Crear un controlador mediante físicas, que se mueva en el plano XZ. v
    /// 2. Crear bordes de colisión en la escena. v
    /// 3. Crear un spawn mediante colliders que genere otros objetos dentro de los límites de la escena diseñada. v
    /// 4. Dotar a esos objetos de comportamientos mediante físicas. v
    /// 
    /// 
    /// </summary>

    Rigidbody rb;
    private void Start() {
        rb = GetComponent<Rigidbody>();
    }
    private void FixedUpdate()
    {
        float v = Input.GetAxisRaw("Vertical");
        float h = Input.GetAxisRaw("Horizontal");
        Vector3 force = new Vector3(h, 0, v);
        rb.AddForce(force * 1200 * Time.fixedDeltaTime);

    }
}
