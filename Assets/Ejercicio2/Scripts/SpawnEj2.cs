﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEj2 : MonoBehaviour
{
    public GameObject orb;
    public GameObject slime;
    Rigidbody slrb;
    // Start is called before the first frame update
    void Start()
    {
        slrb = orb.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate(){
        
    }
    private void OnTriggerEnter(Collider other){
        Debug.Log(other.name);
        if(other.name=="Player")
            Instantiate(slime,transform.position,Quaternion.identity);
    }
}
