﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class Ejercicio3 : MonoBehaviour {

    /// <summary>
    ///
    /// 1. Controlador para mover el player en el eje XZ, (Hecho)
    /// 2. Que se pueda saltar (Hecho)
    /// 3. Que haya plataformas. (Hecho)
    /// 4. Una mecánica propia. (Hecho)
    /// 5. Condición de victoria y de derrota. (Hecho)
    /// 
    /// </summary>

    public TextMeshPro score;

    Rigidbody rb;
    public float linealForce;
    public float horizontalForce;
    //public float angularForce;
    public float jumpForce;

    public bool onGround;
    public bool jumping;
    bool shouldJump;
    public int jumps = 0;
    bool move;

    [SerializeField]
    int coins;

    // Start is called before the first frame update
    void Start() {
        rb = GetComponent<Rigidbody>();
        move = false;
        coins = 0;
    }

    public void AllowMove() {
        move = true;
    }

    private void FixedUpdate(){
        if (move)
        {
            float v = Input.GetAxisRaw("Vertical");
            float h = Input.GetAxisRaw("Horizontal");
            if (v < 0) v = 0;
            //Vector3 turn = new Vector3(0, h, 0);
            //Vector3 force = new Vector3(h, 0, v);
            rb.AddForce(v * transform.forward * linealForce * Time.fixedDeltaTime, ForceMode.VelocityChange);
            rb.AddForce(h * transform.right * horizontalForce * Time.fixedDeltaTime, ForceMode.VelocityChange);
            //rb.AddTorque(turn* jumpForce * Time.fixedDeltaTime, ForceMode.VelocityChange);
            if (v == 0) rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y, 0);
            if (h == 0) rb.velocity = new Vector3(0, rb.velocity.y, rb.velocity.z);
            //if (h == 0) rb.angularVelocity = Vector3.zero;
            if (shouldJump) Jump();
        }
        else {
            rb.velocity = new Vector3(0, rb.velocity.y, 0);
        }
    }

    private void Update(){
        if (Input.GetKeyDown(KeyCode.Space))
            if (onGround)// || jumping && jumps < 2)
                shouldJump = true;
    }

    private void Jump() {
        rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        jumping = true;
        shouldJump = false;
        jumps++;
    }

    private void OnCollisionExit(Collision collision){
        if (collision.other.gameObject.layer == 9){
            onGround = false;
        }
    }

    private void OnCollisionEnter(Collision collision){
        if (collision.other.gameObject.layer == 9){
            onGround = true;
            jumps = 0;
            jumping = false;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 10)
        {
            Debug.Log("Perdiste");
            SceneManager.LoadScene(0);
        }
        if (other.gameObject.layer == 11)
        {
            if(coins>=6)
                Debug.Log("Ganaste");
            else
                Debug.Log("Casi, te faltaron: "+(6-coins)+" monedas por recoger");
        }
        if (other.tag == "Coin" || other.tag == "AntiCoin")
        {
            if(other.tag == "Coin") coins++;
            else coins--;
            score.SetText("Monedas: " + coins);
            other.gameObject.SetActive(false);
        }
    }
}
