﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinEj3 : MonoBehaviour
{
    Rigidbody rb;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    private void FixedUpdate()
    {
        rb.AddTorque(Vector3.up * 500 * Time.fixedDeltaTime, ForceMode.VelocityChange);
    }
}
