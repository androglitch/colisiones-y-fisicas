﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager3 : MonoBehaviour
{
    public Ejercicio3 player;
    public GameObject score;
    public GameObject init;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W)) {
            init.SetActive(false);
            score.SetActive(true);
            player.AllowMove();
        }
    }
}
